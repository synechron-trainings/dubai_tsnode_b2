// // Eager Execution
// function idNormal(){
//     console.log("Id is created via normal function");
// }

// // Lazy Execution
// function* idGenerator(){
//     console.log("Id is created via generator function");
// }

// // idNormal();
// // idGenerator(); // It will not execute the function

// var genObj = idGenerator();
// // console.log(genObj);
// console.log(genObj.next());

// // ----------------------------------

// function* idGenerator() {
//     console.log("Generator function execution started...");
//     yield 10;
//     yield 20;
//     yield 30;
//     console.log("Generator function execution completed...");
// }

// var genObj = idGenerator();
// console.log(genObj.next());
// console.log(genObj.next());
// console.log(genObj.next());
// console.log(genObj.next());

// ----------------------------------

// class GQueue<T> {
//     private _data: T[] = [];

//     push(item: T) {
//         this._data.push(item);
//     }

//     pop(): T | undefined {
//         return this._data.shift();
//     }

//     *[Symbol.iterator]() {
//         for (let i = 0; i < this._data.length; i++) {
//             yield this._data[i];
//         }
//     }
// }

class GQueue<T> {
    private _data: T[] = [];

    push(item: T) {
        this._data.push(item);
    }

    pop(): T | undefined {
        return this._data.shift();
    }

    *[Symbol.iterator]() {
        yield* this._data;
    }
}

type GOrder = {
    id: number,
    description: string
}

var ordersGQ = new GQueue<Order>();
ordersGQ.push({ id: 1, description: "Order One" });
ordersGQ.push({ id: 2, description: "Order Two" });
ordersGQ.push({ id: 3, description: "Order Three" });

for (const item of ordersGQ) {
    console.log(item);
}