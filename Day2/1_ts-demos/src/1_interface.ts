// const area = function (shape: { h: number, w?: number }) {
//     shape.w = shape.w || shape.h;
//     return shape.h * shape.w;
// }

// let s1 = { h: 20, w: 10 };
// console.log(area(s1));

// let s2 = { h: 10, w: 10, extra: 20 };
// console.log(area(s2));

// let s3 = { h: 20 };
// console.log(area(s3));

// let s4 = { h: 20, extra: 20 };
// console.log(area(s4));

// // --------------------------------------- Type Alias

// type Shape = { h: number, w?: number };

// const area = function (shape: Shape) {
//     shape.w = shape.w || shape.h;
//     return shape.h * shape.w;
// }

// let s1: Shape = { h: 20, w: 10 };
// console.log(area(s1));

// // let s2: Shape = { h: 10, w: 10, extra: 20 };         // Error: Object literal may only specify known properties, and 'extra' does not exist in type 'Shape'.
// // console.log(area(s2));

// let s3: Shape = { h: 20 };
// console.log(area(s3));

// // let s4: Shape = { h: 20, extra: 20 };                // Error: Object literal may only specify known properties, and 'extra' does not exist in type 'Shape'.
// // console.log(area(s4));

// // --------------------------------------- Interface

// interface IShape 
// { 
//     h: number; 
//     w?: number; 
// }

// const area = function (shape: IShape) {
//     shape.w = shape.w || shape.h;
//     return shape.h * shape.w;
// }

// let s1: IShape = { h: 20, w: 10 };
// console.log(area(s1));

// // let s2: IShape = { h: 10, w: 10, extra: 20 };         // Error: Object literal may only specify known properties, and 'extra' does not exist in type 'Shape'.
// // console.log(area(s2));

// let s3: IShape = { h: 20 };
// console.log(area(s3));

// // let s4: IShape = { h: 20, extra: 20 };                // Error: Object literal may only specify known properties, and 'extra' does not exist in type 'Shape'.
// // console.log(area(s4));

// // --------------------------------------- With Function

// interface IPerson {
//     name: string;
//     age: number;
//     greet(message: string): string;
// }

// let p1: IPerson = {
//     name: "Manish",
//     age: 35,
//     greet: function (message: string) {
//         return `Hello`;
//     }
// };

// let p2: IPerson = {
//     name: "Abhijeet",
//     age: 35,
//     greet: function (message: string) {
//         return `Hola`;
//     }
// };

// console.log(p1.greet("Hello"));
// console.log(p2.greet("Hola"));

// type TPerson = {
//     name: string,
//     age: number,
//     greet(message: string): string
// };

// let p3: TPerson = {
//     name: "Ramakant",
//     age: 35,
//     greet: function (message: string) {
//         return `Hi`;
//     }
// };

// console.log(p3.greet("Hi"));

// // ------------------------------------ Difference between Type and Interface

// // type TShape = {
// //     height: number;
// // };

// // // Duplicate identifier 'TShape'.
// // type TShape = {
// //     width: number;
// // };

// // Interface Merging
// interface IShape {
//     height: number;
// }

// interface IShape {
//     width: number;
// }

// let s1: IShape = {
//     height: 20,
//     width: 10
// };

// // ----------------------------------------------------- Type Alias and Interface Used Together

// interface ICustomer {
//     doShopping(): string;
// }

// interface IEmployee {
//     doWork(): string;
// }

// type TCustomerOrEmployee = ICustomer | IEmployee;       // Union Type (Type Gurad)

// var person1: TCustomerOrEmployee = {
//     doShopping: function () {
//         return `Shopping Done`;
//     }
// };

// type TCustomerAndEmployee = ICustomer & IEmployee;      // Intersection Type

// var person2: TCustomerAndEmployee = {
//     doShopping: function () {
//         return `Shopping Done`;
//     },
//     doWork: function () {
//         return `Work Done`;
//     }
// };