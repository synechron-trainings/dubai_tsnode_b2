// interface IPerson {
//     name: string;
//     age: number;
//     greet(message: string): string;
// }

// class Person implements IPerson {
//     constructor(public name: string, public age: number) { }

//     greet(message: string): string {
//         return `Hello`;
//     }
// }

// let p1: IPerson = new Person("Manish", 35);
// console.log(p1.greet("Hi"));

// let p2: IPerson = new Person("Abhijeet", 35);
// console.log(p2.greet("Hey"));

// // ----------------------------------- Multiple Interface Implementation

// interface IPerson {
//     name: string;
//     age: number;
//     greet(message: string): string;
// }

// interface IEmployee {
//     doWork(): string;
// }

// class Person implements IPerson, IEmployee {
//     constructor(public name: string, public age: number) { }

//     greet(message: string): string {
//         return `Hello`;
//     }

//     doWork(): string {
//         return `Learning TypeScript`;
//     }
// }

// let p1: Person = new Person("Manish", 35);
// console.log(p1.greet("Hi"));
// console.log(p1.doWork());

// // ----------------------------------- Interface Extraction

// interface IPerson {
//     name: string;
//     age: number;
//     greet(message: string): string;
// }

// interface IEmployee {
//     doWork(): string;
// }

// interface ICustomer {
//     doShopping(): string;
// }

// class Person implements IPerson, IEmployee, ICustomer {
//     constructor(public name: string, public age: number) { }

//     greet(message: string): string {
//         return `Hello`;
//     }

//     doWork(): string {
//         return `Learning TypeScript`;
//     }

//     doShopping(): string {
//         return `Shopping in progress`;
//     }
// }

// let p1: Person = new Person("Manish", 35);
// console.log(p1.greet("Hi"));
// console.log(p1.doWork());
// console.log(p1.doShopping());

// // Interface Extraction
// let p2: IPerson = new Person("Abhijeet", 35);
// console.log(p2.greet("Hey"));

// let e1: IEmployee = new Person("Abhijeet", 35);
// console.log(e1.doWork());

// let c1: ICustomer = new Person("Abhijeet", 35);
// console.log(c1.doShopping());

// ----------------------------------- Interface Extension - Interface Inheritance

interface IPerson {
    name: string;
    age: number;
    greet(message: string): string;
}

interface IEmployee extends IPerson {
    doWork(): string;
}

interface ICustomer extends IPerson {
    doShopping(): string;
}

class Person implements IPerson, IEmployee, ICustomer {
    constructor(public name: string, public age: number) { }

    greet(message: string): string {
        return `Hello`;
    }

    doWork(): string {
        return `Learning TypeScript`;
    }

    doShopping(): string {
        return `Shopping in progress`;
    }
}

let e1: IEmployee = new Person("Abhijeet", 35);
console.log(e1.greet('Hi'));
console.log(e1.doWork());

let c1: ICustomer = new Person("Abhijeet", 35);
console.log(c1.greet('Hey'));
console.log(c1.doShopping());