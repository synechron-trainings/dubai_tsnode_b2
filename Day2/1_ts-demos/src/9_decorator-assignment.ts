function CatchAndLog<T>(target: T, propertyKey: string, descriptor: PropertyDescriptor) {
    const originalMethod = descriptor.value;

    descriptor.value = function (...args: any[]) {
        try {
            return originalMethod.apply(this, args);
        } catch (error: any) {
            console.log(`Error occurred in ${propertyKey}, message is:`, error.message);
        }
    }

    return descriptor;
}

class Service {
    @CatchAndLog
    doSomething(data: any) {
        if (!data) {
            throw new Error("Data is required!");
        }
        // Imagine some logic here that could also throw an error
        console.log("Processing data:", data);
        return "Data processed successfully";
    }
}

let service = new Service();
console.log(service.doSomething(null));

// Write a decorator to handle exceptions thrown by the method