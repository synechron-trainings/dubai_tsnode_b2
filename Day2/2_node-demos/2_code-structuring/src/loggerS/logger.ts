class Logger {
    log(message: string) {
        console.log(`${message}, logged using Logger Class Log Method.`);
    }
}

export const logger = new Logger();
