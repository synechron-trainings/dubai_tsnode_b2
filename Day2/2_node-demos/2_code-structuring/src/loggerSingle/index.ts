import { Logger } from './logger';
let logger: Logger | null = null;

export function getLogger() {
    if (logger === null) {
        logger = new Logger();
    }
    return logger;
}