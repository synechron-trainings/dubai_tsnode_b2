// Node way of importing modules
// require('./logger');


// Import module for its exports
// const logger = require('./logger');


// TypeScript way of importing modules
// import './logger';

// Import module for its exports
// import * as logger from './logger';
// console.log(logger);

// --------------------------- Module Resolution
// Modules are loaded only once
// const logger1 = require('./logger');
// const logger2 = require('./logger');

// import * as logger1 from './logger';
// import * as logger2 from './logger';

// console.log(logger1 === logger2);

// const logger1 = require('./logger');
// import * as logger1 from './logger';
// console.log(logger1);

// ---------------------------------------------- Singleton
// import { logger } from './loggerS';
// logger.log('Hello World');

// import { logger as l1 } from './loggerS';
// import { logger as l2 } from './loggerS';
// console.log(l1 === l2);

// import { getLogger } from './loggerSingle';

// const logger1 = getLogger();
// const logger2 = getLogger();

// logger1.log('Hello World');
// logger2.log('Hello World');

// console.log(logger1 === logger2);

// -------------------------------------- Factory Method Pattern
// import { DBLoggerFactory, FileLoggerFactory, LoggerFactory } from "./loggerFactory";

// function clientCode(factory: LoggerFactory) {
//     factory.logMessage('Hello from App');
// }

// clientCode(new DBLoggerFactory());
// clientCode(new FileLoggerFactory());

import { DBLoggerFactory, FileLoggerFactory } from "./loggerFactory";

const dbLogger = DBLoggerFactory.getLogger();
const fileLogger = FileLoggerFactory.getLogger();

dbLogger.log('Hello from App');
fileLogger.log('Hello from App');

const logger1 = DBLoggerFactory.getLogger();
const logger2 = DBLoggerFactory.getLogger();

console.log(logger1 === logger2);