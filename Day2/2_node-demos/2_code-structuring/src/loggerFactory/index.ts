// import { DBLogger } from "./db-logger";
// import { FileLogger } from "./file-logger";
// import { ILogger } from "./i-logger";

// export abstract class LoggerFactory {
//     abstract getLogger(): ILogger;

//     logMessage(message: string): void {
//         this.getLogger().log(message);
//     }
// }

// export class DBLoggerFactory extends LoggerFactory {
//     getLogger(): ILogger {
//         return new DBLogger();
//     }
// }

// export class FileLoggerFactory extends LoggerFactory {
//     getLogger(): ILogger {
//         return new FileLogger();
//     }
// }

import { DBLogger } from "./db-logger";
import { FileLogger } from "./file-logger";
import { ILogger } from "./i-logger";

export class DBLoggerFactory {
    static getLogger(): ILogger {
        return new DBLogger();
    }
}

export class FileLoggerFactory {
    static getLogger(): ILogger {
        return new FileLogger();
    }
}