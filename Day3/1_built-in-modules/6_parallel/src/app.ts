// import './without-worker';      // 13.451s               // Sequential processing    
// import './with-worker';         // 4.424s                // Parallel processing with worker_threads
import './with-child-process';  // 5.249s                   // Parallel processing with child_process