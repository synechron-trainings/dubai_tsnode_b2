// import * as fs from 'fs';
// import * as path from 'path';

// const filePath = path.join(process.cwd(), 'files', 'file1.txt');

// // // Read the entire file into memory and then callback will be executed
// // fs.readFile(filePath, 'utf-8', (err, data) => {
// //     if (err)
// //         console.error(err);
// //     else
// //         console.log(data.toString());
// // });

// // const readStream = fs.createReadStream(filePath, {
// //     highWaterMark: 1024 * 1024          
// // });

// const readStream = fs.createReadStream(filePath, 'utf-8');

// readStream.on('open', () => {
//     console.log('Stream opened');
// });

// readStream.on('error', (err: NodeJS.ErrnoException) => {
//     console.error(err.message);
// });

// readStream.on('data', (chunk: string) => {
//     console.log(chunk);
// });

// readStream.on('end', () => {
//     console.log('Stream ended');
// });

// ----------------------------------------------- File Copy

import * as fs from 'fs';
import * as path from 'path';

const readFilePath = path.join(process.cwd(), 'files', 'file1.txt');
const writeFilePath = path.join(process.cwd(), 'files', 'file2.txt');

const readStream = fs.createReadStream(readFilePath, 'utf-8');
const writeStream = fs.createWriteStream(writeFilePath, 'utf-8');

readStream.on('data', (chunk: string) => {
    writeStream.write(chunk);
});

readStream.on('end', () => {
    console.log('File Copied....');
});

readStream.on('error', (err: NodeJS.ErrnoException) => {
    console.error('Error reading file:', err);
});

writeStream.on('error', (err: NodeJS.ErrnoException) => {
    console.error('Error writing file:', err);
});