// // ----------------------------------------------- File Copy

// import * as fs from 'fs';
// import * as path from 'path';

// const readFilePath = path.join(process.cwd(), 'files', 'file1.txt');
// const writeFilePath = path.join(process.cwd(), 'files', 'file2.txt');

// const readStream = fs.createReadStream(readFilePath, 'utf-8');
// const writeStream = fs.createWriteStream(writeFilePath, 'utf-8');

// readStream.on('data', (chunk: string) => {
//     writeStream.write(chunk);
// });

// readStream.on('end', () => {
//     console.log('File Copied....');
// });

// readStream.on('error', (err: NodeJS.ErrnoException) => {
//     console.error('Error reading file:', err);
// });

// writeStream.on('error', (err: NodeJS.ErrnoException) => {
//     console.error('Error writing file:', err);
// });

// // ----------------------------------------------- File Copy using Pipe

// import * as fs from 'fs';
// import * as path from 'path';

// const readFilePath = path.join(process.cwd(), 'files', 'file1.txt');
// const writeFilePath = path.join(process.cwd(), 'files', 'file2.txt');

// const readStream = fs.createReadStream(readFilePath, 'utf-8');
// const writeStream = fs.createWriteStream(writeFilePath, 'utf-8');

// readStream.pipe(writeStream).on('finish', () => {
//     console.log('File Copied....');
// });

// ----------------------------------------------- File Compression using Pipe

import * as fs from 'fs';
import * as path from 'path';
import * as zlib from 'zlib';
import { compareFileSizes } from './file-stats';

const readFilePath = path.join(process.cwd(), 'files', 'file1.txt');
const writeFilePath = path.join(process.cwd(), 'files', 'file1.txt.gz');

const readStream = fs.createReadStream(readFilePath, 'utf-8');
const writeStream = fs.createWriteStream(writeFilePath, 'utf-8');

readStream
    .pipe(zlib.createGzip())
    .pipe(writeStream)
    .on('finish', () => {
        console.log('File Compressed....');
        compareFileSizes(readFilePath, writeFilePath);
    });