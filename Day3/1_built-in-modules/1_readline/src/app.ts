// import * as readline from 'readline';
// // console.log(readline);

// const rl = readline.createInterface({
//     input: process.stdin,
//     output: process.stdout
// });

// // rl.question('What is your name? ', (name) => { 
// //     console.log(`Hello, ${name}!`);
// //     rl.close();
// // });

// // console.log("This is the last line of the code.");

// // rl.question('Enter the first number: ', (input1) => { 
// //     rl.question('Enter the second number: ', (input2) => { 
// //         let sum = parseInt(input1) + parseInt(input2);
// //         console.log(`The sum of ${input1} and ${input2} is ${sum}`);
// //         rl.close();
// //     }); 
// // });

// // Convert the above code to promise based implementation

// // function enterNumberOne() {
// //     return new Promise((resolve, reject) => {
// //         rl.question('Enter the first number: ', (input1) => {
// //             resolve(input1);
// //         });
// //     });
// // }

// // function enterNumberTwo() {
// //     return new Promise((resolve, reject) => {
// //         rl.question('Enter the second number: ', (input2) => {
// //             resolve(input2);
// //         });
// //     });
// // }

// // function add([n1, n2]) {
// //     var sum = parseInt(n1) + parseInt(n2);
// //     console.log(`The sum of ${n1} and ${n2} is ${sum}`);
// //     rl.close();
// // }

// // enterNumberOne().then((n1) => {
// //     enterNumberTwo().then((n2) => {
// //         add([n1, n2]);
// //     });
// // });

// // // ----------------------------- Promise Chaining

// // function enterNumberOne(): Promise<string> {
// //     return new Promise((resolve, reject) => {
// //         rl.question('Enter the first number: ', (input1) => {
// //             resolve(input1);
// //         });
// //     });
// // }

// // function enterNumberTwo(n1: string): Promise<[string, string]> {
// //     return new Promise((resolve, reject) => {
// //         rl.question('Enter the second number: ', (input2) => {
// //             resolve([n1, input2]);
// //         });
// //     });
// // }

// // function add([n1, n2]: [string, string]) {
// //     var sum = parseInt(n1) + parseInt(n2);
// //     console.log(`The sum of ${n1} and ${n2} is ${sum}`);
// //     rl.close();
// // }

// // enterNumberOne().then(enterNumberTwo).then(add);

// // ----------------------------- Async Await

// function enterNumber(message: string): Promise<number> {
//     return new Promise((resolve, reject) => {
//         rl.question(message, (input) => {
//             resolve(parseInt(input));
//         });
//     });
// }

// (async function () {
//     let n1 = await enterNumber('Enter the first number: ');
//     let n2 = await enterNumber('Enter the second number: ');
//     let sum = n1 + n2;
//     console.log(`The sum of ${n1} and ${n2} is ${sum}`);
//     rl.close();
// })();

import './understanding-promise';