You are tasked with creating a Node.js program that interacts with the user via the command line interface using the 
readline module. The program should allow the user to input data, which will be saved to a file. 
The user should have the option to exit the program and save the entered data by pressing Ctrl+C.

Requirements:
1. Prompt the user to input data using the command line interface.
2. Allow the user to enter multiple lines of data until they decide to exit.
3. Provide an option for the user to exit the program by pressing Ctrl+C.
4. Confirm with the user if they want to exit
5. Save the collected data into a specified file after every newline (Enter key pressed).
6. Handle errors gracefully and provide appropriate error messages if any issues occur during the process.

Your task is to create a Node.js program that fulfills the requirements outlined above.