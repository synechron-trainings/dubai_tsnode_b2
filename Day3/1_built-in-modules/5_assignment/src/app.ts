import * as fs from 'fs';
import * as path from 'path';
import * as readline from 'readline';

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const writeFilePath = path.join(process.cwd(), 'files', 'output.txt');
const writeStream = fs.createWriteStream(writeFilePath, 'utf-8');

console.log("Enter your data. Press Enter to save each line. Press Ctrl+C to exit.");

rl.on('line', (input: string) => {
    if (input.trim() === '') {
        console.log('Please enter some data or press Ctrl+C to exit.');
        return;
    }

    writeStream.write(input + '\n', (err) => {
        if (err) {
            console.error('Error writing to file:', err);
        } else {
            console.log('Data saved to', writeFilePath);
        }
    });
});

rl.on('SIGINT', () => {
    rl.question('Are you sure you want to exit? (yes/no): ', (answer) => {
        if (answer.trim().toLowerCase() === 'yes') {
            rl.close();
        } else {
            console.log('Continuing to enter data.');
            rl.prompt();
        }
    });
});

rl.on('close', () => {
    console.log('Exiting...');
    writeStream.end();
    process.exit(0);
});