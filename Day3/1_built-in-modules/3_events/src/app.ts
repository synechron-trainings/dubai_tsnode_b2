import { StringEmitter } from "./string-emitter";

const stringEmitter = StringEmitter.getInstance();

// const str = stringEmitter.getString();
// console.log(str);

// setInterval(() => {
//     const str = stringEmitter.getString();
//     console.log(str);
// }, 2000);

// -------------------------------------- Callback

// stringEmitter.pushString((str) => {
//     console.log(str);
// });

// stringEmitter.pushString((str) => {
//     console.log("S1: ", str);
// });

// stringEmitter.pushString((str) => {
//     console.log("S2: ", str);
// });

// -------------------------------------- Event

// stringEmitter.on('stringEmitted', (str) => {
//     console.log(str);
// });

stringEmitter.on('stringEmitted', (str) => {
    console.log("S1: ", str);
});

// stringEmitter.on('stringEmitted', (str) => {
//     console.log("S2: ", str);
// });

let count  = 0;

function printString(str: string) {
    console.log("S2: ", str);
    count++;
    if(count === 3) {
        stringEmitter.off('stringEmitted', printString);            // Unsubscribe
    }
}

stringEmitter.on('stringEmitted', printString);                     // Subscribe