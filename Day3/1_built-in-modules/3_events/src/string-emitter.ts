import { EventEmitter } from 'events';

export class StringEmitter extends EventEmitter {
    private stringArray: string[];
    private static instance: StringEmitter;

    private constructor() {
        super();
        this.stringArray = ['NodeJS', 'ReactJS', 'AngularJS', 'VueJS', 'EmberJS'];
        this.run();
    }

    private run(): void {
        setInterval(() => {
            let randomString = this.stringArray[Math.floor(Math.random() * this.stringArray.length)];
            this.emit('stringEmitted', randomString);
        }, 2000);
    }

    public static getInstance(): StringEmitter {
        if (!StringEmitter.instance) {
            StringEmitter.instance = new StringEmitter();
        }

        return StringEmitter.instance;
    }

    public pushString(callback: (str: string) => void): void {
        setInterval(() => {
            let randomString = this.stringArray[Math.floor(Math.random() * this.stringArray.length)];
            callback(randomString);
        }, 2000);
    }

    public getString(): string {
        let randomString = this.stringArray[Math.floor(Math.random() * this.stringArray.length)];
        return randomString;
    }
}