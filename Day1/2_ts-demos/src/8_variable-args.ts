// -------------------------- REST PARAMETER --------------------------
// Rest parameter is used to pass a variable number of arguments to a function.
// Rest parameter is prefixed with three dots (...)
// Rest parameter is an array of elements of a particular type.
// Rest parameter must be the last parameter in the parameter list.

function Average(...args: number[]) {
    if(args.length > 0)
        return args.reduce((a, b) => a + b) / args.length;
    else
        return 0;
}

console.log(Average());
console.log(Average(1));
console.log(Average(1, 2));
console.log(Average(1, 2, 3, 4, 5));
console.log(Average(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));

// Combine comma seperated items into a Array (...) - Rest Parameter
// ... used in function parameter at the time of function creation - Rest Parameter
// ... on Left hand side of assignment operator - Rest Parameter

var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
// console.log(Average(numbers));          // Error: Argument of type 'number[]' is not assignable to parameter of type 'number'
console.log(Average(...numbers));          // Spread Operator - It will spread the array into individual elements

// Split Array/Object to a comma seperated items - Spread Operator
// ... used in function argument at the time of function call - Spread Operator
// ... on Right hand side of assignment operator - Spread Operator