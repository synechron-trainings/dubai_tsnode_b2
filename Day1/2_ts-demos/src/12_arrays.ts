// // Implicitly Typed Arrays
// var arr1 = [10, 20, 30, 40, 50];
// var arr2 = ["A", "B", "C", "D"];
// var arr3 = [10, "A", 20, "B", 30, "C"];

// // Explicitly Typed Arrays
// var arr4: number[];
// arr4 = [10, 20, 30, 40, 50];

// var arr5: Array<number>;
// arr5 = [10, 20, 30, 40, 50];

// var arr6: Array<number> = [10, 20, 30, 40, 50];

// var arr7 = new Array<number>();
// console.log(arr7);
// console.log(arr7.length);

// var arr8 = new Array<number>(5);
// console.log(arr8);
// console.log(arr8.length);

// var arr9 = new Array<string>("Manish");
// console.log(arr9);
// console.log(arr9.length);

// var arr10 = Array.of(5);
// console.log(arr10);
// console.log(arr10.length);

// var arr = [1, 2, 3, 4, 5];

// var arr11 = new Array(arr);
// console.log(arr11);
// console.log(arr11.length);

// var arr12 = Array.from(arr);
// console.log(arr12);
// console.log(arr12.length);

// var arr13 = [...arr];
// console.log(arr13);
// console.log(arr13.length);

// -------------------------- Array of Objects

// var empList: Array<{id: number, name:string, city: string}>;

// empList = [
//     {id: 101, name: "Manish", city: "Pune"},
//     {id: 102, name: "Ramesh", city: "Mumbai"},
//     {id: 103, name: "Suresh", city: "Nagpur"},
//     {id: 104, name: "Dinesh", city: "Nashik"},
//     {id: 105, name: "Ganesh", city: "Aurangabad"}
// ];

// empList.push({id: 106, name: "Mahesh", city: "Solapur"});
// console.log(empList);

// ------------------------- Type Alias

type Employee = { id: number, name: string, city: string };

var empList: Array<Employee>;

empList = [
    { id: 101, name: "Manish", city: "Pune" },
    { id: 102, name: "Ramesh", city: "Mumbai" },
    { id: 103, name: "Suresh", city: "Nagpur" },
    { id: 104, name: "Dinesh", city: "Nashik" },
    { id: 105, name: "Ganesh", city: "Aurangabad" }
];

empList.push({ id: 106, name: "Mahesh", city: "Solapur" });
empList.push({ id: 107, name: "Pravin", city: "Solapur" });
// console.log(empList);

// delete empList[2];

// // empList.forEach((item, index) => {
// //     console.log(`Item at ${index} is: `, item);
// // });

// // for (let i = 0; i < empList.length; i++) {
// //     console.log(`Item at ${i} is: `, empList[i]);
// // }

// // for(let i in empList) {
// //     console.log(`Item at ${i} is: `, empList[i]);
// // }

// for(let emp of empList) {
//     console.log(emp);
// }

// --------------------------- Array Methods

let r1 = empList.find(emp => emp.id === 103);
console.log(r1);

let r2 = empList.findIndex(emp => emp.id === 103);
console.log(r2);

let names = empList.map(emp => emp.name.toUpperCase());
console.log(names);

let ids = empList.map(emp => emp.id);
console.log(ids);

let sum = ids.reduce((a, b) => a + b);
console.log(sum);

var cities = empList.map(emp => emp.city);
console.log(cities);

var uniqueCities = Array.from(new Set(cities));
console.log(uniqueCities);