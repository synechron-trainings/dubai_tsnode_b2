let d1: number;
d1 = 10;

let d2: string;
d2 = "Hello";

let d3: any;
d3 = 10;
d3 = "Hello";
d3 = true;

// Type Guard
let d4: number | string;
d4 = 10;
d4 = "Manish";
// d4 = true;              // Error

let d5: number | string | boolean;
d5 = 10;
d5 = "Manish";
d5 = true;