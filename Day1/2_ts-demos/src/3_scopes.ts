// Global Scope (If module is disabled in tsconfig.json)
// Namespace Scope
// Module Scope (If module is enabled in tsconfig.json)
// Function Scope
// Block Scope - let / const

// var a = "Manish";

// namespace Demo {
//     var a = "Manish";
//     console.log(a);
// }

// console.log("Outside",  a);

// namespace Demo {
//     export var a = "Manish";
//     console.log(a);
// }

// console.log("Outside",  Demo.a);

// export var a = "Manish";

// export function display() {
//     console.log(a);
// }

// export class Employee {
//     constructor() {
//         console.log(a);
//     }
// }

// var i = 20;
// var i = 30;
// console.log(i);

// var j = 20;
// var j = "Manish";       // Error: "Subsequent variable declarations must have the same type."

// var Supports Hoisting - Moving Declarations to the top
// a1 = "Hello";
// console.log(a1);
// var a1;

// var i = 20;                 // Global or Namaspace or Module Scope

// // function test() {
// //     console.log("Inside Function, i is:", i);
// // }

// // function test() {
// //     var i = "Hello";        // Function Scope
// //     console.log("Inside Function, i is:", i);
// // }

// // var does not support Block Scoping
// function test() {
//     if (true) {
//         var i = "Hello";        // Function Scope
//         console.log("Inside Block, i is:", i);
//     }
//     console.log("Inside Function, i is:", i);
// }

// test();
// console.log("Outside Function, i is:", i);

// var i = 100;
// console.log("Before Loop, i is:", i);

// for (var i = 0; i < 5; i++) {
//     console.log("Inside Loop, i is:", i);
// }

// console.log("After Loop, i is:", i);

// --------------------------------------------------- Let Keyword ---------------------------------------------------

// let i = 20;
// // let i = 30;         // Error: Cannot redeclare block-scoped variable 'i'
// console.log(i);

// let doest not Supports Hoisting - Moving Declarations to the top
// a1 = "Hello";
// console.log(a1);
// let a1;

// // let support Block Scoping
// var i = 100;
// console.log("Before Loop, i is:", i);

// for (let i = 0; i < 5; i++) {
//     console.log("Inside Loop, i is:", i);
// }

// console.log("After Loop, i is:", i);

// ----------------------------- Const Keyword

let j: number;
j = 10;
j = 20;

// const env: string;         // Error: 'const' declarations must be initialized.

const env: string = "Development";
console.log(env);

// env = "Production";         // Error: Cannot assign to 'env' because it is a constant.
// console.log(env);

if (true) {
    const env: string = "Production";
    console.log("Block Scope, env is:", env);
}