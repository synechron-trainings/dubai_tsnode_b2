// // function hello() {
// //     console.log('Hello World');
// // }

// // var r = hello();
// // console.log(r);
// // console.log(typeof r);

// // var r1: undefined;
// // // r1 = 10;        // Error: Type '10' is not assignable to type 'undefined'
// // r1 = undefined;
// // r1 = null;
// // r1 = void 0;
// // console.log(r1);

// // var r2: void;
// // // r2 = 10;        // Error: Type '10' is not assignable to type 'void'
// // r2 = undefined;
// // r2 = null;
// // r2 = void 0;
// // console.log(r2);

// // var r3: null;
// // // r3 = 10;        // Error: Type '10' is not assignable to type 'null'
// // r3 = undefined;
// // r3 = null;
// // r3 = void 0;
// // console.log(r3);

// // var r4: never;
// // // r4 = 10;        // Error: Type '10' is not assignable to type 'never'
// // // r4 = undefined; // Error: Type 'undefined' is not assignable to type 'never'
// // // r4 = null;      // Error: Type 'null' is not assignable to type 'never'
// // // r4 = void 0;    // Error: Type 'void' is not assignable to type 'never'
// // console.log(r4);

// // var r5: any;
// // console.log(r5.trim());             // No Compile time check are done on Any type (But it will give runtime error)

// // var r6: unknown;
// // console.log(r6.trim());             // Error: Property 'trim' does not exist on type 'unknown'


// // JavaScript - undefined, null, void
// // TypeScript - never, any, unknown

// // ---------------------------------------------------------

// // function iterate() {
// //     let i = 1;

// //     while (true) {
// //         console.log(i++);
// //     }
// // }

// function iterate(): never {
//     let i = 1;

//     while (true) {
//         console.log(i++);
//     }
// }

// iterate();      // Infinite loop
// console.log("Last line of the program");        // This line will never be executed

// ---------------------------------------------------------

//  Function Declaration
function add1(x: number, y: number): number {
    return x + y;
}

// Function Expression
let add2 = function (x: number, y: number): number {
    return x + y;
}

let add3: (a: number, b: number) => number;
add3 = function (x: number, y: number): number {
    return x + y;
}

let add4: (a: number, b: number) => number = function (x: number, y: number): number {
    return x + y;
}

let add5: (a: number, b: number) => number;
add5 = function (x, y) {
    return x + y;
}

// Multiline Lambda
let add6: (a: number, b: number) => number;
add6 = (x, y) => {
    return x + y;
}

// Single line Lambda
let add7: (a: number, b: number) => number;
add7 = (x, y) => x + y;

console.log(add1(2, 3));
console.log(add2(2, 3));
console.log(add3(2, 3));
console.log(add4(2, 3));
console.log(add5(2, 3));
console.log(add6(2, 3));
console.log(add7(2, 3));