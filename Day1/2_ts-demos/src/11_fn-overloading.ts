// function hello() {
//     console.log("Hello World!");
// }

// // Error: Duplicate function implementation.
// function hello(name: string) {
//     console.log("Hello, " + name + "!");
// }

// hello();
// hello("Synechron");

// ------------------------------------------------- Function Overloading

function hello(): void;
function hello(name: string): void;

function hello(...args: string[]): void {
    if (args.length === 0) {
        console.log("Hello World!");
    } else {
        console.log("Hello, " + args.join(", ") + "!");
    }
}

hello();
hello("Synechron");
// hello("Synechron", "Technologies");         // Error: Expected 1 arguments, but got 2