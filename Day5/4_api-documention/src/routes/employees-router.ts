// import express from 'express';
// import * as employeeController from '../controllers/employees-controller';

// const router = express.Router();

// /**
//  * @swagger
//  * tags:
//  *  name: Employees
//  *  description: API endpoints for employee management
//  */

// /**
//  * @swagger
//  * /employees:
//  *  get:
//  *      summary: List all the employees
//  *      tags: [Employees]
//  *      responses:
//  *          '200':
//  *              description: Returns a list of employees
//  */
// router.get('/', employeeController.getAllEmployees);

// /**
//  * @swagger
//  * /employees/{empid}:
//  *  get:
//  *      summary: Get an employee by Id
//  *      tags: [Employees]
//  *      parameters: 
//  *          - in: path
//  *            name: empid
//  *            required: true
//  *            description: Id of the employee to get
//  *            schema:
//  *              type: integer  
//  *      responses:
//  *          '200':
//  *              description: Returns a single employee
//  *          '404':
//  *              description: Employee not found
//  */
// router.get('/:empid', employeeController.getEmployeeDetails);

// /**
//  * @swagger
//  * /employees:
//  *  post:
//  *      summary: Create a new Employee
//  *      tags: [Employees]
//  *      requestBody:
//  *          required: true
//  *          content:
//  *              application/json: 
//  *                  schema:
//  *                      type: object
//  *                      properties:
//  *                          eid:
//  *                              type: integer
//  *                          ename:
//  *                              type: string  
//  *      responses:
//  *          '201':
//  *              description: Employee created successfully
//  *          '500':
//  *              description: Employee creation failed
//  */
// router.post('/', employeeController.createEmployee);

// /**
//  * @swagger
//  * /employees:
//  *  put:
//  *      summary: Update an employee by Id
//  *      tags: [Employees]
//  *      parameters: 
//  *          - in: path
//  *            name: empid
//  *            required: true
//  *            description: Id of the employee to edit
//  *            schema:
//  *              type: integer  
//  *      requestBody:
//  *          required: true
//  *          content:
//  *              application/json: 
//  *                  schema:
//  *                      type: object
//  *                      properties:
//  *                          eid:
//  *                              type: integer
//  *                          ename:
//  *                              type: string  
//  *      responses:
//  *          '200':
//  *              description: Employee updated successfully
//  *          '404':
//  *              description: Employee not found
//  */
// router.put('/:empid', employeeController.updateEmployee);

// /**
//  * @swagger
//  * /employees:
//  *  delete:
//  *      summary: Delete an employee by Id
//  *      tags: [Employees]
//  *      parameters: 
//  *          - in: path
//  *            name: empid
//  *            required: true
//  *            description: Id of the employee to edit
//  *            schema:
//  *              type: integer  
//  *      responses:
//  *          '200':
//  *              description: Employee deleted successfully
//  *          '404':
//  *              description: Employee not found
//  */
// router.delete('/:empid', employeeController.deleteEmployee);

// export default router;


// ----------------------------------------------------------------------------- Use swagger.json
import express from 'express';
import * as employeeController from '../controllers/employees-controller';

const router = express.Router();

router.get('/', employeeController.getAllEmployees);

router.get('/:empid', employeeController.getEmployeeDetails);

router.post('/', employeeController.createEmployee);

router.put('/:empid', employeeController.updateEmployee);

router.delete('/:empid', employeeController.deleteEmployee);

export default router;