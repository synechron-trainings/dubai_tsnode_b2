import { Request, Response } from 'express';

export const index = (req: Request, res: Response) => {
    res.render('home/index', { pageTitle: 'Index Page' });
}

export const about =  (req: Request, res: Response) => {
    res.render('home/about', { pageTitle: 'About Page' });
};

export const contact =  (req: Request, res: Response) => {
    res.render('home/contact', { pageTitle: 'Contact Page' });
};