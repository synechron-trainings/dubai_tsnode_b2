import express, { Request, Response, NextFunction } from 'express';
import logger from 'morgan';
import favicon from 'serve-favicon';
import path from 'path';
import 'dotenv/config';
import './config/data-source';

import indexRouter from './routes/index-router';
import employeesRouter from './routes/employees-router';

const app = express();

app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(process.cwd(), 'public')));
app.use(favicon(path.join(process.cwd(), 'public', 'images', 'favicon.png')));

app.use('/', indexRouter);
app.use('/employees', employeesRouter);

app.use((error: Error, req: Request, res: Response, next: NextFunction) => {
    res.locals.message = error.message;
    res.locals.error = req.app.get('env') === 'development' ? error : {};
    res.status(500);
    res.render('error', { pageTitle: 'Error Page' });
});

export default app;