import { Request, Response } from 'express';
import { getEmployees, getEmployee, insertEmployee, findAndUpdateEmployee, findAndDeleteEmployee } from '../data-access';
import { Employee } from '../models/employee';
import { employeeSchema } from '../validations/schema';

export const getAllEmployees = async (req: Request, res: Response) => {
    try {
        const employees = await getEmployees();
        res.status(200).json({ employees });
    } catch (error) {
        res.status(500).json({ message: 'Failed to get employees', error });
    }
}

export const getEmployeeDetails = async (req: Request, res: Response) => {
    try {
        const id = parseInt(req.params.empid);
        const employee = await getEmployee(id);

        if (employee) {
            res.status(200).json({ employee });
        } else {
            res.status(404).json({ message: "Employee not found" });
        }

    } catch (error) {
        res.status(500).json({ message: 'Failed to get employee details', error });
    }
}

// export const createEmployee = async (req: Request, res: Response) => {
//     try {
//         const { eid, ename } = req.body;
//         const employee = new Employee(parseInt(eid), ename);
//         await insertEmployee(employee);
//         res.status(201).json({ message: 'Employee created successfully', employee });
//     } catch (error) {
//         res.status(500).json({ message: 'Failed to create employee', error });
//     }
// }

export const createEmployee = async (req: Request, res: Response) => {
    try {
        const { eid, ename } = await employeeSchema.validateAsync(req.body);
        const employee = new Employee(parseInt(eid), ename);
        await insertEmployee(employee);
        res.status(201).json({ message: 'Employee created successfully', employee });
    } catch (error) {
        res.status(500).json({ message: 'Failed to create employee', error });
    }
}

// export const updateEmployee = async (req: Request, res: Response) => {
//     const id = parseInt(req.params.empid);

//     try {
//         const { eid, ename } = req.body;
//         const employee = new Employee(parseInt(eid), ename);
//         await findAndUpdateEmployee(id, employee);
//         res.status(200).json({ message: 'Employee updated successfully', employee });
//     } catch (error) {
//         res.status(500).json({ message: 'Failed to update employee', error });
//     }
// }


export const updateEmployee = async (req: Request, res: Response) => {
    const id = parseInt(req.params.empid);

    try {
        const { eid, ename } = await employeeSchema.validateAsync(req.body);
        const employee = new Employee(parseInt(eid), ename);
        await findAndUpdateEmployee(id, employee);
        res.status(200).json({ message: 'Employee updated successfully', employee });
    } catch (error) {
        res.status(500).json({ message: 'Failed to update employee', error });
    }
}

export const deleteEmployee = async (req: Request, res: Response) => {
    try {
        const id = parseInt(req.params.empid);
        await findAndDeleteEmployee(id);
        // res.status(204).end();
        res.status(200).json({ message: 'Employee deleted successfully' });
    } catch(error) {
        res.status(500).json({ message: 'Failed to delete employee', error });
    }
}