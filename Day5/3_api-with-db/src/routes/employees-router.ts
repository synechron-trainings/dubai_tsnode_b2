import express from 'express';
import * as employeeController from '../controllers/employees-controller';

const router = express.Router();

// List all employees
// GET - http://localhost:3000/employees
router.get('/', employeeController.getAllEmployees);

// Get a single employee by Id
// GET - http://localhost:3000/employees/1
router.get('/:empid', employeeController.getEmployeeDetails);

// Create Employee
// POST - http://localhost:3000/employees + JSON data in Body
router.post('/', employeeController.createEmployee);

// Edit Employee
// PUT - http://localhost:3000/employees/1 + JSON data in Body
router.put('/:empid', employeeController.updateEmployee);

// Delete Employee
// DELETE - http://localhost:3000/employees/1
router.delete('/:empid', employeeController.deleteEmployee);

export default router;