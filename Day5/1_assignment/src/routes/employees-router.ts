import express from 'express';
import * as employeeController from '../controllers/employees-controller';

const router = express.Router();

router.get('/', employeeController.renderIndex);

router.get('/details/:empid', employeeController.renderEmployeeDetails);

router.get('/create', employeeController.renderCreateEmployee);

router.post('/create', employeeController.createEmployee);

router.get('/edit/:empid', employeeController.renderEditEmployee);

router.post('/edit/:empid', employeeController.updateEmployee);

router.get('/delete/:empid', employeeController.renderDeleteEmployee);

router.post('/delete/:empid', employeeController.deleteEmployee);

export default router;