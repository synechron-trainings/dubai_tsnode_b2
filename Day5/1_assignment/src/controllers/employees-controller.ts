import { Request, Response } from 'express';
import { getEmployees, getEmployee, insertEmployee, findAndUpdateEmployee, findAndDeleteEmployee } from '../data-access';
import { Employee } from '../models/employee';

export const renderIndex = (req: Request, res: Response) => {
    getEmployees().then(employees => {
        res.render('employees/index', { pageTitle: 'Employees Page', empList: employees, message: '' });
    }).catch(message => {
        res.render('employees/index', { pageTitle: 'Employees Page', empList: [], message });
    });
}

export const renderEmployeeDetails = (req: Request, res: Response) => {
    const id = parseInt(req.params.empid);

    getEmployee(id).then(employee => {
        res.render('employees/details', { pageTitle: 'Employee Details Page', employee, message: '' });
    }).catch(message => {
        res.render('employees/details', { pageTitle: 'Employee Details Page', employee: {}, message });
    });
}

export const renderCreateEmployee = (req: Request, res: Response) => {
    res.render('employees/create', { pageTitle: 'Create Employee Page', employee: {}, message: '' });
}

export const createEmployee = (req: Request, res: Response) => {
    const { eid, ename } = req.body;
    const employee = new Employee(parseInt(eid), ename);

    insertEmployee(employee).then(() => {
        res.redirect('/employees');
    }).catch(message => {
        res.render('employees/create', { pageTitle: 'Create Employee Page', employee, message });
    });
}

export const renderEditEmployee = (req: Request, res: Response) => {
    const id = parseInt(req.params.empid);

    getEmployee(id).then(employee => {
        res.render('employees/edit', { pageTitle: 'Employee Edit Page', employee, message: '' });
    }).catch(message => {
        res.render('employees/edit', { pageTitle: 'Employee Edit Page', employee: {}, message });
    });
}

export const updateEmployee = (req: Request, res: Response) => {
    const id = parseInt(req.params.empid);

    const { eid, ename } = req.body;
    const employee = new Employee(parseInt(eid), ename);

    findAndUpdateEmployee(id, employee).then(() => {
        res.redirect('/employees');
    }).catch(message => {
        res.render('employees/edit', { pageTitle: 'Employee Edit Page', employee, message });
    });
}

export const renderDeleteEmployee = (req: Request, res: Response) => {
    const id = parseInt(req.params.empid);

    getEmployee(id).then(employee => {
        res.render('employees/delete', { pageTitle: 'Employee Delete Page', employee, message: '' });
    }).catch(message => {
        res.render('employees/delete', { pageTitle: 'Employee Delete Page', employee: {}, message });
    });
}

export const deleteEmployee = (req: Request, res: Response) => {
    const id = parseInt(req.params.empid);

    findAndDeleteEmployee(id).then(employee => {
        res.redirect('/employees');
    }).catch(message => {
        res.render('employees/delete', { pageTitle: 'Employee Delete Page', employee: {}, message });
    });
}