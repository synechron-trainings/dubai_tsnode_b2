// import * as https from 'https';
// import { ClientRequest, IncomingMessage } from 'http';
// import * as path from 'path';
// import * as fs from 'fs';

// const url: string = 'https://jsonplaceholder.typicode.com/posts';

// const filePath: string = path.join('.', 'files', 'posts.json');
// const fileStream: fs.WriteStream = fs.createWriteStream(filePath, 'utf-8');

// const requestOptions: https.RequestOptions = {
//     method: 'GET',
//     headers: {
//         'Content-Type': 'application/json',
//         'Accept': 'application/json'
//     }
// };

// const request: ClientRequest = https.request(url, requestOptions, (response: IncomingMessage) => {
//     if (response.statusCode !== 200) {
//         console.error('Error occurred while fetching data');
//         return;
//     }

//     response.on('data', (chunk: Buffer) => {
//         // console.log("Data Received, ", chunk.length, " bytes");
//         fileStream.write(chunk);
//     });

//     response.on('end', () => {
//         console.log("Data Received Successfully");
//         fileStream.end();
//     });
// });

// request.end();

// --------------------------------------------- Mapping Response to TypeScript Object ---------------------------------------------

import * as https from 'https';
import { ClientRequest, IncomingMessage } from 'http';
import * as path from 'path';
import * as fs from 'fs';

const url: string = 'https://jsonplaceholder.typicode.com/posts';

const filePath: string = path.join('.', 'files', 'posts.json');

interface Post {
    userId: number;
    id: number;
    title: string;
    body: string;
}

const requestOptions: https.RequestOptions = {
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
};

const request: ClientRequest = https.request(url, requestOptions, (response: IncomingMessage) => {
    if (response.statusCode !== 200) {
        console.error('Error occurred while fetching data');
        return;
    }

    let dataChunks: Buffer[] = [];

    response.on('data', (chunk: Buffer) => {
        dataChunks.push(chunk);
    });

    response.on('end', () => {
        const data = Buffer.concat(dataChunks).toString();
        const posts: Post[] = JSON.parse(data);
        console.log(posts);

        console.log('All data received and parsed....');
        fs.writeFileSync(filePath, JSON.stringify(posts, null, 2));

        console.log("\n First Post: ");
        console.log("Id: ", posts[0].id);
        console.log("Title: ", posts[0].title);
        console.log("Body: ", posts[0].body);
    });
});

request.end();