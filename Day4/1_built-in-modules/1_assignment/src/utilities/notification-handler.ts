import { Account } from "src/account";
import { NotificationService } from "./notification-service";
import { NotificationServiceRegistry } from "./notification-service-registry";

export class NotificationHandler {
    constructor(private notificationServices: NotificationService[]) { }

    listen(account: Account) {
        account.on('depositSuccess', (balance: number) => {
            this.notify(`Deposit Success, Balance is: ${balance}`);
        });

        account.on('withdrawSuccess', (balance: number) => {
            this.notify(`Withdraw Success, Balance is: ${balance}`);
        });

        account.on('withdrawFailure', (balance: number) => {
            this.notify(`Withdraw Failure, Balance is: ${balance}`);
        });
    }

    private notify(message: string) {
        this.notificationServices.forEach(service => {
            service.send(message);
        });
    }
}

export class NotificationHandlerFactory {
    static createHandler(): NotificationHandler {
        const notificationServices = NotificationServiceRegistry.getRegisteredServices();
        return new NotificationHandler(notificationServices);
    }
}