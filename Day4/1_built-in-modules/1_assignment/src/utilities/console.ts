import { NotificationService } from "./notification-service";

export class Console implements NotificationService {
    private static instance: Console;

    private constructor() { }

    static getInstance(): Console {
        if (!Console.instance) {
            Console.instance = new Console();
        }

        return Console.instance;
    }

    send(message: string) {
        console.log(message);
    }
}

export const consoleService = Console.getInstance();