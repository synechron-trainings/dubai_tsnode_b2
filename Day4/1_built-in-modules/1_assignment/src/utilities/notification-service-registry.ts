import { NotificationService } from "./notification-service";

export class NotificationServiceRegistry {
    private static services: NotificationService[] = [];

    private constructor() { }

    static registerService(service: NotificationService) {
        this.services.push(service);
    }

    static getRegisteredServices(): NotificationService[] {
        return this.services;
    }
}