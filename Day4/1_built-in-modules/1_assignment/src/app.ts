import { AccountFactory } from "./account";
import { configureNotificationServices } from "./config/notification-service-config";
import { NotificationHandlerFactory } from "./utilities/notification-handler";

configureNotificationServices();
const notificationHandler = NotificationHandlerFactory.createHandler();

let a1 = AccountFactory.createAccount(1, 1000, 0.05, notificationHandler);

a1.deposit(1000);
a1.withdraw(2000);
a1.withdraw(1000);