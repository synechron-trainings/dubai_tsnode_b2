import { smsService } from '../utilities/sms';
import { consoleService } from '../utilities/console';
import { NotificationServiceRegistry } from '../utilities/notification-service-registry';
import { emailService } from '../utilities/email';

export function configureNotificationServices() {
    NotificationServiceRegistry.registerService(consoleService);
    NotificationServiceRegistry.registerService(smsService);
    NotificationServiceRegistry.registerService(emailService);
}