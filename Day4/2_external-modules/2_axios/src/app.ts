import axios, { AxiosResponse } from 'axios';
import * as path from 'path';
import * as fs from 'fs';

const url: string = 'https://jsonplaceholder.typicode.com/posts';
const filePath: string = path.join('.', 'files', 'posts.json');

interface Post {
    userId: number;
    id: number;
    title: string;
    body: string;
}

// axios.get<Array<Post>>(url).then((response: AxiosResponse<Array<Post>>) => {
//     const posts: Post[] = response.data;
//     console.log(posts);

//     console.log('All data received and parsed....');
//     fs.writeFileSync(filePath, JSON.stringify(posts, null, 2));

//     console.log("\n First Record:");
//     console.log("Id: ", posts[0].id);
//     console.log("Title: ", posts[0].title);
//     console.log("Body: ", posts[0].body);
// }).catch((error: any) => {
//     console.error(`Error: ${error}`);
// });

(async function () {
    try {
        const response: AxiosResponse<Array<Post>> = await axios.get<Array<Post>>(url);
        const posts: Post[] = response.data;
        console.log(posts);

        console.log('All data received and parsed....');
        fs.writeFileSync(filePath, JSON.stringify(posts, null, 2));

        console.log("\n First Record:");
        console.log("Id: ", posts[0].id);
        console.log("Title: ", posts[0].title);
        console.log("Body: ", posts[0].body);
    } catch (error) {
        console.error(`Error: ${error}`);
    }
})();