import { Server, createServer } from 'http';
import express, { Request, Response } from 'express';
import path from 'path';

const app = express();

const employees = [
    { id: 1, name: "Manish" },
    { id: 2, name: "Abhijeet" },
    { id: 3, name: "Ram" },
    { id: 4, name: "Abhishek" },
    { id: 5, name: "Ramakant" }
];

app.get('/', (req: Request, res: Response) => {
    res.sendFile(path.join(process.cwd(), 'public', 'index.html'));
});

app.get('/about', (req: Request, res: Response) => { 
    res.sendFile(path.join(process.cwd(), 'public', 'about.html'));
});

app.get('/contact', (req: Request, res: Response) => { 
    res.sendFile(path.join(process.cwd(), 'public', 'contact.html'));
});

app.get('/data', (req: Request, res: Response) => { 
    const data = employees;
    res.json(data);
});

// ---------------------- Hosting Code
const server: Server = createServer(app);

server.listen(3000);

function onError(error: Error) {
    console.error('Error occurred while starting the server', error);
}

function onListening() {
    const address = server.address();
    const port = typeof address === 'string' ? address : address?.port;
    console.log(`Express Server started listening on port ${port}`);
}

server.on('error', onError);
server.on('listening', onListening);