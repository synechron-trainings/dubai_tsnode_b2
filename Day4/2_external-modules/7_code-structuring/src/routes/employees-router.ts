import express from 'express';
import * as employeeController from '../controllers/employees-controller';

const router = express.Router();

router.get('/', employeeController.renderIndex);

router.get('/details/:empid', employeeController.renderEmployeeDetails);

export default router;