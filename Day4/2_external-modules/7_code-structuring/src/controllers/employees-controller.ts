import { Request, Response } from 'express';
import { getEmployees, getEmployee } from '../data-access';

export const renderIndex = (req: Request, res: Response) => {
    res.render('employees/index', { pageTitle: 'Employees Page', empList: getEmployees() });
}

export const renderEmployeeDetails = (req: Request, res: Response) => {
    const id = parseInt(req.params.empid);
    res.render('employees/details', { pageTitle: 'Employee Details Page', employee: getEmployee(id) });
}