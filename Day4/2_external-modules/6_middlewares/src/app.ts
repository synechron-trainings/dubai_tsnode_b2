import { Server, createServer } from 'http';
import express, { Request, Response, NextFunction } from 'express';
import logger from 'morgan';
import favicon from 'serve-favicon';
import path from 'path';

const app = express();

app.set('view engine', 'pug');

const employees = [
    { id: 1, name: "Manish" },
    { id: 2, name: "Abhijeet" },
    { id: 3, name: "Ram" },
    { id: 4, name: "Abhishek" },
    { id: 5, name: "Ramakant" }
];

app.use(logger('dev'));
app.use(favicon(path.join(process.cwd(), 'public', 'images', 'favicon.png')));

// app.use((req: Request, res: Response, next: NextFunction) => {
//     console.log("Request - Middleware One");
//     next();
//     console.log("Response - Middleware One");
// });

// app.use((req: Request, res: Response, next: NextFunction) => {
//     console.log("Request - Middleware Two");
//     next();
//     console.log("Response - Middleware Two");
// });

// app.use((req: Request, res: Response, next: NextFunction) => {
//     const startTime = new Date().getTime();
//     next();
//     res.on('finish', () => {
//         const endTime = new Date().getTime();
//         console.log(`Request URL: ${req.url} - Total Time: ${endTime - startTime} ms`);
//     });
// });

app.get('/', (req: Request, res: Response) => {
    // console.log("Request Handler Executed");
    // throw new Error("Error occurred in request handler");
    res.render('index', { pageTitle: 'Index Page' });
});

app.get('/about', (req: Request, res: Response) => {
    res.render('about', { pageTitle: 'About Page' });
});

app.get('/contact', (req: Request, res: Response) => {
    res.render('contact', { pageTitle: 'Contact Page' });
});

app.get('/employees', (req: Request, res: Response) => {
    res.render('employees/index', { pageTitle: 'Employees Page', empList: employees });
});

app.use((error: Error, req: Request, res: Response, next: NextFunction) => {
    res.locals.message = error.message;
    res.locals.error = req.app.get('env') === 'development' ? error : {};
    res.status(500);
    res.render('error', { pageTitle: 'Error Page' });
});


// ---------------------- Hosting Code
const server: Server = createServer(app);

server.listen(3000);

function onError(error: Error) {
    console.error('Error occurred while starting the server', error);
}

function onListening() {
    const address = server.address();
    const port = typeof address === 'string' ? address : address?.port;
    console.log(`Express Server started listening on port ${port}`);
}

server.on('error', onError);
server.on('listening', onListening);