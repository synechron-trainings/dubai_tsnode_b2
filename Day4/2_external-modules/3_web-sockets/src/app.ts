import { Server, createServer, IncomingMessage, ServerResponse } from 'http';
import * as fs from 'fs';
import * as path from 'path';
import { server as WebSocketServer } from 'websocket';
import { stringEmitter } from './string-emitter';

const server: Server = createServer((request: IncomingMessage, response: ServerResponse) => {
    const indexPath = path.join(process.cwd(), 'public', 'index.html');

    fs.readFile(indexPath, (error: NodeJS.ErrnoException | null, data: Buffer) => {
        if (error) {
            response.statusCode = 404;
            response.setHeader('content-type', 'text/html');
            response.write('<h1>Resource not found</h1>');
            response.end();
        } else {
            response.statusCode = 200;
            response.setHeader('content-type', 'text/html');
            response.write(data);
            response.end();
        }
    });
});

server.listen(3000, () => {
    const address = server.address();
    const port = typeof address === 'string' ? address : address?.port;
    console.log(`Server started listening on port ${port}`);
});

server.on('error', (error: Error) => {
    console.error('Error occurred while starting the server', error);
});

// ------------------------------ WebSocket Code Starts
let count = 1;
let clients: { [id: number]: Client } = {};

stringEmitter.on('data', (data: string) => {
    Object.values(clients).forEach((client) => {
        client.sendUTF(data);
    });
});

interface Client {
    sendUTF(message: string): void;
}

const wsServer = new WebSocketServer({
    httpServer: server
});

wsServer.on('request', (request) => {
    const connection = request.accept('echo-protocol');

    const id = count++;
    clients[id] = connection;
    console.log(`Connection Accepted: [Client ${id}]`);

    connection.on('message', (message) => {
        if (message.type === 'utf8') {
            console.log(`Received message from [Client ${id}]: ${message.utf8Data}`);
            connection.sendUTF(`Hello from Server`);
        }
    });

    connection.on('close', () => {
        delete clients[id];
        console.log(`Connection Closed: [Client ${id}]`);
    });
});