import { employeeDAO } from '../data-access';
import { Employee } from '../models';
import { employeeSchema } from '../validations';
import { BadRequestError, Body, Delete, Get, HttpCode, JsonController, NotFoundError, Param, Post, Put } from 'routing-controllers';

@JsonController('/employees')
export class EmployeeController {
    @Get('/')
    async getEmployees() {
        try {
            const employees = await employeeDAO.getEmployees();
            return { employees };
        } catch (error) {
            throw new Error('Failed to get employees');
        }
    }

    @Get('/:empid')
    async getEmployeeDetails(@Param('empid') empid: number) {
        const employee = await employeeDAO.getEmployee(empid);

        if (employee) {
            return employee;
        } else {
            throw new NotFoundError('Employee not found');
        }
    }

    @Post('/')
    @HttpCode(201)
    async createEmployee(@Body() body: any) {
        try {
            const value = await employeeSchema.validateAsync(body);
            const employee = new Employee(value.eid, value.ename);
            const insertedEmployee = await employeeDAO.insertEmployee(employee);
            return { message: 'Employee created successfully', employee: insertedEmployee };
        } catch (error: any) {
            if (error.isJoi) {
                throw new BadRequestError('Invalid user data provided');
            }
            throw new Error('Failed to create employee');
        }
    }

    @Put('/:empid')
    async updateEmployee(@Param('empid') empid: number, @Body() body: any) {
        try {
            const value = await employeeSchema.validateAsync(body);
            const employee = new Employee(value.eid, value.ename);
            const updatedEmployee =  await employeeDAO.findAndUpdateEmployee(empid, employee);
            return { message: 'Employee updated successfully', employee: updatedEmployee };
        } catch (error: any) {
            if (error.isJoi) {
                throw new BadRequestError('Invalid user data provided');
            }
            throw new Error('Failed to update employee');
        }
    }

    @Delete('/:empid')
    @HttpCode(204)
    async deleteEmployee(@Param('empid') empid: number) {
        try {
            await employeeDAO.findAndDeleteEmployee(empid);
            return { message: 'Employee deleted successfully' };
        } catch (error) {
            throw error;
        }
    }
}