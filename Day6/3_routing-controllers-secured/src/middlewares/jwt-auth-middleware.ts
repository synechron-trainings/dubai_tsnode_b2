import { expressjwt } from 'express-jwt';

export const authenticateJWT = expressjwt({ secret: process.env.TOKEN_SECRET, algorithms: ['HS256'] });

// import {Request, Response, NextFunction} from 'express';
// import * as jwt from 'jsonwebtoken';

// export function authenticateJWT(req: Request, res: Response, next: NextFunction) {
//     const authHeader = req.headers.authorization;

//     if (authHeader) {
//         const token = authHeader.split(' ')[1]; // Assumes Bearer scheme

//         // Verifying the token
//         jwt.verify(token, process.env.TOKEN_SECRET as string, (err, user) => {
//             if (err) {
//                 return res.sendStatus(403); // Forbidden access
//             }

//             // Attaching the user payload to the request, if needed
//             req.user = user;
//             next();
//         });
//     } else {
//         res.sendStatus(401); // Unauthorized access
//     }
// }