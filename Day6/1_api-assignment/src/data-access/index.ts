import { employeeDAO } from "./employee-dao";
import { userDAO } from "./user-dao";

export { employeeDAO, userDAO };
