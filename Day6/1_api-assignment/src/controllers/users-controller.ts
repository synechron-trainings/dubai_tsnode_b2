import { Request, Response } from 'express';
import { User } from '../models';
import { userDAO } from '../data-access';
import { userSchema } from '../validations';
import { ValidationError } from 'joi';

export const getUsers = async (req: Request, res: Response) => {
    try {
        const page = req.query.page ? parseInt(req.query.page as string) : undefined;
        const limit = req.query.limit ? parseInt(req.query.limit as string) : undefined;

        const { users, total } = await userDAO.getUsers(page, limit);
        res.status(200).json({ users, totalPages: Math.ceil(total / (limit || 10)) });
    } catch (error) {
        res.status(500).json({ message: 'Failed to get users', error });
    }
}

export const getUserDetails = async (req: Request, res: Response) => {
    try {
        const id = parseInt(req.params.userid);
        const user = await userDAO.getUser(id);

        if (user) {
            res.status(200).json({ user });
        } else {
            res.status(404).json({ message: "User not found" });
        }
    } catch (error) {
        res.status(500).json({ message: 'Failed to get user details', error });
    }
}

export const createUser = async (req: Request, res: Response) => {
    try {
        const value = await userSchema.validateAsync(req.body);
        const user = new User(value.userid, value.name, value.email);
        const insertedUser = await userDAO.insertUser(user);
        res.status(201).json({ message: 'User created successfully', user: insertedUser });
    } catch (error) {
        const validationError = error as ValidationError;
        res.status(400).json({ message: 'Validation failed', error: validationError.details });
    }
}

export const updateUser = async (req: Request, res: Response) => {
    const id = parseInt(req.params.userid);

    try {
        const value = await userSchema.validateAsync(req.body);
        const user = new User(value.userid, value.name, value.email);
        const updatedUser = await userDAO.findAndUpdateUser(id, user);

        if (updatedUser) {
            res.status(200).json({ message: 'User updated successfully', user: updatedUser });
        } else {
            res.status(404).json({ message: 'User not found' });
        }
    } catch (error) {
        const validationError = error as ValidationError;
        res.status(400).json({ message: 'Validation failed', error: validationError.details });
    }
}

export const deleteUser = async (req: Request, res: Response) => {
    try {
        const id = parseInt(req.params.userid);
        await userDAO.findAndDeleteUser(id);
        res.status(200).json({ message: 'User deleted successfully' });
    } catch (error) {
        res.status(500).json({ message: 'Failed to delete user', error });
    }
}