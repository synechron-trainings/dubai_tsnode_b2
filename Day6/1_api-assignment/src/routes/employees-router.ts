import express from 'express';
import * as employeeController from '../controllers/employees-controller';

const router = express.Router();

router.get('/', employeeController.getAllEmployees);

router.get('/:empid', employeeController.getEmployeeDetails);

router.post('/', employeeController.createEmployee);

router.put('/:empid', employeeController.updateEmployee);

router.delete('/:empid', employeeController.deleteEmployee);

export default router;