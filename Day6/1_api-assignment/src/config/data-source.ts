import { DataSource } from "typeorm";
import "reflect-metadata";
import { EmployeeEntity, UserEntity } from "../entities";

export const AppDataSource: DataSource = new DataSource({
    type: "mongodb",
    url: process.env.mongoURI,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    database: "Dubai_B2",
    entities: [EmployeeEntity, UserEntity],
    synchronize: true,
    logging: true
});

(function () {
    console.log("Connecting to the Database, please wait...");

    AppDataSource.initialize().then(() => {
        console.log("DataSource has been initialized successfully.");
    }).catch((err) => {
        console.error("Error during Data Source initialization:", err.message);
        process.exit(0);
    });
})();