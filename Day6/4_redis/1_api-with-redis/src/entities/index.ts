import { EmployeeEntity } from "./employee";
import { UserEntity } from "./user";

export { EmployeeEntity, UserEntity };