import { User } from "../models";
import { AppDataSource } from "../config/data-source";
import { UserEntity } from "../entities";

const userRepository = AppDataSource.getRepository(UserEntity);

export const getUsers = async (page: number | undefined, limit: number | undefined): Promise<{ users: Array<User>, total: number }> => {
    let skip: number | undefined = undefined;
    let take: number | undefined = undefined;

    if (page !== undefined && limit !== undefined) {
        skip = (page - 1) * limit;
        take = limit;
    }

    let users;
    let total;

    if (skip !== undefined && take !== undefined) {
        [users, total] = await userRepository.findAndCount({ skip, take });
    } else {
        [users, total] = await userRepository.findAndCount();
    }

    return {
        users: users.map((entity) => new User(entity.userId, entity.username, entity.email)),
        total: total
    };
}

export const getUser = async (id: number): Promise<User | null> => {
    const userEntity = await userRepository.findOneBy({ userId: id });
    if (userEntity) {
        return new User(userEntity.userId, userEntity.username, userEntity.email);
    }
    return null;
}

export const insertUser = async (userToInsert: User): Promise<User> => {
    const userEntity = userRepository.create({
        userId: userToInsert.id,
        username: userToInsert.name,
        email: userToInsert.email
    });

    const insertedUser = await userRepository.save(userEntity);
    return new User(insertedUser.userId, insertedUser.username, insertedUser.email);
}

export const findAndUpdateUser = async (id: number, userToUpdate: User): Promise<User | null> => {
    let userEntity = await userRepository.findOneBy({ userId: id });
    if (userEntity) {
        // userEntity = userRepository.merge(userEntity, userToUpdate);
        userEntity.userId = userToUpdate.id ?? userEntity.userId;
        userEntity.username = userToUpdate.name ?? userEntity.username;
        userEntity.email = userToUpdate.email ?? userEntity.email;
        const updatedUser = await userRepository.save(userEntity);
        return new User(updatedUser.userId, updatedUser.username, updatedUser.email);
    }
    return null;
}

export const findAndDeleteUser = async (id: number): Promise<void> => {
    let userEntity = await userRepository.findOneBy({ userId: id });
    if (userEntity) {
        await userRepository.remove(userEntity);
    } else {
        throw new Error('User not found');
    }
}

export const userDAO = {
    getUsers,
    getUser,
    insertUser,
    findAndUpdateUser,
    findAndDeleteUser
};