import express from 'express';
import logger from 'morgan';
import favicon from 'serve-favicon';
import path from 'path';
import 'dotenv/config';
import './config/data-source';
import swaggerUi from 'swagger-ui-express';
import * as swaggerDocument from './config/swagger.json';
import { useExpressServer } from 'routing-controllers';
import 'reflect-metadata';
import cors from 'cors';

// Code for Creating Express Server using routing-controllers
// import { createExpressServer } from 'routing-controllers';

// const app = createExpressServer({
//     controllers: [__dirname + '/controllers/*.ts'],
//     middlewares: [__dirname + '/middlewares/*.ts']
// });

const app = express();

const corsOptions = {
    origin: '*', // Configure this to allow specific origins
    optionsSuccessStatus: 200, // For legacy browser support
    methods: "GET, HEAD, PUT, PATCH, POST, DELETE",
    allowedHeaders: ['Content-Type', 'Authorization'],
    credentials: true // This allows session cookies from the browser to pass through
};

app.use(cors(corsOptions));

app.set('view engine', 'pug');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(process.cwd(), 'public')));
app.use(favicon(path.join(process.cwd(), 'public', 'images', 'favicon.png')));

useExpressServer(app, {
    controllers: [__dirname + '/controllers/*.ts'],
    middlewares: [__dirname + '/middlewares/*.ts']
});

export default app;