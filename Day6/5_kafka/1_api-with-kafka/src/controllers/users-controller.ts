import { User } from '../models';
import { userDAO } from '../data-access';
import { userSchema } from '../validations';
import { Body, Delete, Get, JsonController, NotFoundError, Param, Post, Put, QueryParam, UseBefore } from 'routing-controllers';
import { authenticateJWT } from '../middlewares/jwt-auth-middleware';
import KafkaProducer from "../config/kafka";

@JsonController('/users')
@UseBefore(authenticateJWT)
export class UserController {
    private producer: KafkaProducer;

    constructor() {
        this.producer = KafkaProducer.getInstance();
        this.producer.start();
    }

    @Get('/')
    async getUsers(@QueryParam("page") page: number, @QueryParam("limit") limit: number) {
        try {
            const result = await userDAO.getUsers(page, limit);

            await this.producer.sendBatch(result.users.map((user) => {
                return {
                    id: user.id,
                    name: user.name,
                    email: user.email
                };
            }));

            return {
                users: result.users,
                totalPages: Math.ceil(result.total / limit),
                message: 'Data retrieved successfully and sent to Kafka Producer'
            };
        } catch (error) {
            console.error('Error fetching user data:', error);
            throw error;
        }
    }

    @Get('/:userid')
    async getUserDetails(@Param("userid") userid: number) {
        const user = await userDAO.getUser(userid);
        if (user) {
            return user;
        } else {
            throw new NotFoundError(`User was not found.`);
        }
    }

    @Post('/')
    async createUser(@Body() body: any) {
        try {
            const value = await userSchema.validateAsync(body);
            const user = new User(value.userid, value.name, value.email);
            const insertedUser = await userDAO.insertUser(user);

            return { message: 'User created successfully', user: insertedUser };
        } catch (error) {
            throw error;
        }
    }

    @Put('/:userid')
    async updateUser(@Param('userid') userid: number, @Body() body: any) {
        try {
            const value = await userSchema.validateAsync(body);
            const user = new User(value.userid, value.name, value.email);
            const updatedUser = await userDAO.findAndUpdateUser(userid, user);

            return { message: 'User updated successfully', user: updatedUser };
        } catch (error) {
            throw error;
        }
    }

    @Delete('/:userid')
    async deleteUser(@Param('userid') userid: number) {
        try {
            await userDAO.findAndDeleteUser(userid);

            return { message: 'User deleted successfully' };
        } catch (error) {
            throw error;
        }
    }
}