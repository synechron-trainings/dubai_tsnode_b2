import KafkaConsumer from "./config/kafka";

export async function readMessages(lastId = "$") {
    try {
        const consumer = KafkaConsumer.getInstance();
        await consumer.start();
    } catch (error) {
        console.error('Error reading messages from Kafka topic:', error);
    }
}